import axios from 'axios'

export default ({ Vue }) => {
  Vue.prototype.$axios = axios
  // axios.defaults.baseURL = 'http://localhost/fuel-watch-api/public/' // LOCAL DB
  axios.defaults.baseURL = 'https://www.findyoursmdc.com/fuel-watch-api/public/' // UP DB
}

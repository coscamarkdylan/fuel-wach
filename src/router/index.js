/* eslint-disable */
import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'
import axios from 'axios'
import routes from './routes'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSession from 'vue-session'

Vue.use(VeeValidate)
Vue.use(VueRouter)
Vue.use(VueSession, { persist: true})
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyANqhG44r39LJ9R0s70EEs5qjLwHzGntX0',
    libraries: 'places'
  },
})

window.axios = axios

Vue.mixin({
  data () {
    return {

      myLoading: {
        show: (message) =>{
          window.showloading = this.$q.loading.show({
            spinner: 'QSpinnerGears',
            spinnerColor: 'orange',
            messageColor: 'orange',
            message: message
          });
        },
        hide: () => {
          this.$q.loading.hide()
        }
      }

    }
  }

})

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
}

/* eslint-disable */
const routes = [
  {
    path: '/',
    name: 'auth', 
    component: () => import('layouts/authLayout.vue'),
    children: [
      { path: '', component: () => import('pages/auth/login.vue') }
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/dashboardLayout.vue'),
    children: [
      { 
        path: 'home', 
        name: 'home', 
        component: () => import('pages/dashboard/home.vue') 
      }, 
      { 
        path: 'logout', 
        name: 'logout', 
        component: () => import('pages/dashboard/logout.vue') 
      },
      { 
        path: 'profile', 
        name: 'profile', 
        component: () => import('pages/dashboard/profile.vue'),
      },
      { 
        path: 'profile-edit',
        name: 'profile-edit',
        component: () => import('pages/dashboard/profile-edit.vue') 
      },
      { 
        path: 'help',
        name: 'help',
        component: () => import('pages/dashboard/help.vue') 
      }

      
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
